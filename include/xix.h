#ifndef XIX_H
#define XIX_H

#include <stddef.h>

#define PKGS_SECTION_NAME           "## Packages"
#define CONF_SECTION_NAME           "## Configurations"
#define SHELL_SUBSECTION            "### Login Shell"
#define GROUPS_SUBSECTION           "### User Groups"
#define SERVICES_SUBSECTION         "### Services"
#define SETUP_SCRIPTS_SUBSECTION    "### Setup Scripts"
#define BLOCK_INDICATOR             "```"

#define MALLOC_ERROR "Failed to allocate memmory"

typedef enum {
    SUCCESS, DISABLED
} ExitCodes;

struct UserData
{
    const char* login_shell;
    const char* home_directory;
};

struct Config
{
    char *current_pkgs;
    char *current_shell;
    char *current_groups;
    char *current_services;
    char *current_scripts;
    char *desired_pkgs;
    char *desired_shell;
    char *desired_groups;
    char *desired_services;
    char *desired_scripts;
};

// config.c
struct Config* get_config_instance();

// handler.c
int handle_packages();
int handle_configurations();

// io.c
const char* get_login_shell();
char* add_line(char* config, const char* section, const char* subsection, const char* line);
char* read_config_file();

// parser.c
char* parse_section(const char *section_name,const char *input);
char* parse_content_block(const char *block_indicator, const char *input);
char* parse_packages(const char *config);
char* parse_packages(const char *config);
char* parse_shell(const char *config);
char* parse_groups(const char *config);
char* parse_services(const char *config);
char* parse_scripts(const char *config);

// utils.c
size_t current_line_length(const char *string);
char* dynamic_strcat(char **dest, const char *src);
char* reallocated(char *string);
void advance_to_next_line(const char **string);
char* allocate_empty_string(size_t size);
size_t get_prefix_length(const char *str);
char* grep_lines(const char *prefix, const char *string);
int count_lines(const char *string);
void io_error_exit(const char *error_typ);

// xbps.c
int  get_explicitly_installed_pkgs(char **pkgs);

#endif
