cmake_minimum_required(VERSION 3.10)
project(xix)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

set(EXECUTABLE_SOURCES
    src/main.c
    src/config.c
    src/handler.c
    src/parser.c
    src/utils.c
    src/io.c
    src/xbps.c
)

add_executable(xix ${EXECUTABLE_SOURCES})

target_include_directories(xix PRIVATE include)

target_link_libraries(xix xbps)

# Generate template.h from template.md
file(READ ${CMAKE_SOURCE_DIR}/resources/template.md FILE_DATA HEX)
string(REGEX REPLACE "(..)" "\\\\x\\1" FILE_DATA ${FILE_DATA})
file(WRITE ${CMAKE_BINARY_DIR}/template.h
     "const char config_template[] = {\"${FILE_DATA}\"};")

# Ensure the generated header is found
include_directories(${CMAKE_BINARY_DIR})
