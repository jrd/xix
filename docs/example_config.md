# Void Linux System Configuration

This file combines package installations and system configurations into a
single, maintainable document. `xix` processes this file to ensure your system
aligns with it. Keep this file updated and adhere to the provided format and
guidelines for the `xix` script to function correctly.

## Packages

### Base System
Essential system packages and utilities for a basic Void Linux setup.
```
base-system             # Void Linux base system meta package
base-devel              # Void Linux development tools meta package
void-repo-nonfree       # Void Linux drop-in file for the nonfree repository
xtools                  # Opinionated helpers for working with XBPS
grub-x86_64-efi         # GRand Unified Bootloader 2 - x86_64 EFI support
linux6.2                # Linux kernel and modules (6.2 series)
```

### Intel Firmware
Firmware updates and drivers for Intel-based systems.
```
intel-ucode             # Microcode update files for Intel CPUs
```

### Session and System Components
Packages for managing user sessions and seat allocation.
```
seatd                   # seat management daemon
pam_rundir              # PAM Module to manage user runtime directories
```

### Intel Graphics
Graphics drivers and utilities for Intel-based systems.
```
mesa-intel-dri          # Mesa DRI drivers for Intel GPUs
intel-video-accel       # Intel Video Acceleration meta-pkg
```

### Wayland Display
Packages related to the Wayland display server and its associated tools.
```
wlroots-devel           # Modular Wayland compositor library
xdg-desktop-portal-wlr  # Backend of xdg-desktop-portal for wlroots
xorg-server-xwayland    # Nested X server that runs as a wayland client
yambar                  # Modular status panel
kanshi                  # Output profiles on hotplug
wbg                     # Simple wallpaper application for Wayland compositors
fnott                   # Keyboard driven and lightweight notification daemon
fuzzel                  # Application launcher
foot                    # Wayland terminal emulator
slurp                   # Select a region in a Wayland compositor
grim                    # Grab images from a Wayland compositor
swappy                  # Wayland native snapshot editing tool
```

### Audio & Video
Tools and utilities for audio and video playback, recording, and conversion.
```
pipewire                # The PipeWire media server
libspa-bluetooth        # libraries for PipeWire bluetooth plugins
ffmpeg                  # Decoding, encoding and streaming software
yt-dlp                  # CLI program to download videos
imv                     # Image viewer
mpv                     # Video player
```

### Wireless
Managing Wi-Fi and Bluetooth connections
```
iwd                     # Internet wireless daemon
bluez                   # Bluetooth tools and daemons
```

### Printing and Scanning
Packages for managing printers and scanners.
```
system-config-printer   # CUPS printer configuration tool and status applet
gutenprint              # Top quality printer drivers for POSIX systems
hplip                   # HP Linux Imaging and Printing
foomatic-db-nonfree     # OpenPrinting printer support - nonfree database
simple-scan             # GTK Simple scanning utility
```

### System Tools
Utilities for monitoring and managing system resources.
```
zsh                     # The Z shell
lf                      # Terminal file manager
exa                     # Modern replacement for ls
btdu                    # Sampling disk usage profiler for btrfs
neofetch                # Simple system information script
htop                    # Interactive process viewer
btop                    # Monitor of resources
pv                      # Monitor the progress of data through a pipeline
zstd                    # Fast real-time compression algorithm - CLI tool
upower                  # Abstraction for enumerating power devices
brightnessctl           # Read and control device brightness
pamixer                 # Pulseaudio command line mixer
pavucontrol             # gui volume control
ncpamixer               # Ncurses mixer for PulseAudio
```

### Encryption Tools
Tools for encryption and secure communication.
```
cryptsetup              # Setup virtual encryption devices under Linux dm-crypt
pass-otp                # Pass extension for managing one-time-password tokens
```

### Development Tools
Software for programming, design, and content creation.
```
neovim                  # Text editor
shellcheck              # Static analysis tool for shell scripts
LibreCAD                # 2D Computer-aided design (CAD) software
blender                 # 3D graphics creation suite
ImageMagick             # Create, edit, compose, or convert bitmap images
gimp                    # GNU image manipulation program
nodejs                  # Evented I/O for V8 javascript
```

### Office Tools
Productivity tools for office tasks and document management.
```
libreoffice             # Productivity suite
zathura-pdf-mupdf       # Document viewer
okular                  # KDE Document Viewer
pandoc                  # Universal converter between markup formats
texlive-bin             # TeX Live Binary distribution through tl-install
```

### Web and Communication
Web browsers and internet communication tools.
```
firefox                 # Mozilla Firefox web browser
chromium                # Google's attempt at creating a web browser
Signal-Desktop          # Signal Private Messenger for Linux
```

### Fonts
Font packages for system-wide usage.
```
nerd-fonts              # Iconic font aggregator, collection and patcher
xorg-fonts              # Modular Xorg Fonts
```

## Configurations

### Groups
This list defines the user's associated groups; all other will be
disassociated.
```
audio                   # Access/control audio devices
video                   # Access video devices/acceleration
scanner                 # Use scanner devices/software
_seatd                  # Manage seat resources with seatd
wheel                   # Perform administrative tasks (e.g., sudo)
bluetooth               # Bluetooth device access and control
```

### Services
This list defines the enabled runit-handled system services; all
others are disabled.
```
dhcpcd                  # Manage network connections (DHCP)
iwd                     # Wireless network management
bluetoothd              # Manage Bluetooth devices
cupsd                   # Handle print jobs and queues
seat                    # Manage session and seat resources
```

### Shell
This setting defines the user's default shell.
```
/bin/zsh                # The Z Shell
```

### Global Configs
Set PAM to handle runtime directory:
```
"-session optional pam_rundir.so"             -> /etc/pam.d/system-login
```
Set the config directory for zsh:
```
"export ZDOTDIR=/home/$USER_NAME/.config/zsh" -> /etc/zsh/zshenv
```

### Unsorted Packages
```
calibre                 # Ebook management application
cups                    # Common Unix Printing System
foomatic-db             # OpenPrinting printer support - database
fzf                     # Command-line fuzzy finder
ghostscript             # Interpreter for the PostScript language
git                     # Git Tree History Storage Tool
gnupg                   # GNU Privacy Guard (2.x)
kdeconnect              # Multi-platform app that allows your devices to commun
mlocate                 # Implementation of locate/updatedb that reuses the dat
nextcloud-client        # NextCloud Desktop client
openresolv              # Management framework for resolv.conf
openvpn                 # Easy-to-use, robust, and highly configurable VPN
pass                    # Stores, retrieves, generates, and synchronizes passwo
passff-host             # Host app for the WebExtension PassFF
pkg-config              # System for managing library compile/link flags
qt5-wayland             # Cross-platform application and UI framework (QT5) - W
qt6-wayland             # Cross-platform application and UI framework - Wayland
rsync                   # Remote fast incremental file transfer tool
speedtest-cli           # Command line bandwidth test using speedtest.net
sway                    # Tiling Wayland compositor compatible with i3
unrar                   # Unarchiver for .rar files (non-free version)
wl-clipboard            # Wayland clipboard utilities
wlr-randr               # Wayland clone of xrandr for wlroots compositors
xmirror                 # Interactive script for changing XBPS mirrors
zip                     # Create/update ZIP files compatible with pkzip
```

### Unsorted Packages
```
YACReader               # Cross-platform reader and manager for your digital co
calibre                 # Ebook management application
cups                    # Common Unix Printing System
foliate                 # Simple and modern GTK eBook reader
foomatic-db             # OpenPrinting printer support - database
fzf                     # Command-line fuzzy finder
ghostscript             # Interpreter for the PostScript language
git                     # Git Tree History Storage Tool
gnupg                   # GNU Privacy Guard (2.x)
mlocate                 # Implementation of locate/updatedb that reuses the dat
nextcloud-client        # NextCloud Desktop client
openresolv              # Management framework for resolv.conf
openvpn                 # Easy-to-use, robust, and highly configurable VPN
pass                    # Stores, retrieves, generates, and synchronizes passwo
passff-host             # Host app for the WebExtension PassFF
pkg-config              # System for managing library compile/link flags
qt5-wayland             # Cross-platform application and UI framework (QT5) - W
qt6-wayland             # Cross-platform application and UI framework - Wayland
rsync                   # Remote fast incremental file transfer tool
sway                    # Tiling Wayland compositor compatible with i3
unrar                   # Unarchiver for .rar files (non-free version)
wl-clipboard            # Wayland clipboard utilities
wlr-randr               # Wayland clone of xrandr for wlroots compositors
xmirror                 # Interactive script for changing XBPS mirrors
zip                     # Create/update ZIP files compatible with pkzip
```
