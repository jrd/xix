# Void Linux System Configuration

This file was generated during the initial execution of xix. Its contents
reflect the current state of the system and user configuration at that time.
For a comprehensive understanding of all available options, please refer to the
example provided in the documentation.

Note 1: The title above (preceded by #) and any descriptive text (such as this
one) can be freely modified or deleted. Section and subsection names (preceded
by ## or ###) and the contents of the code blocks (enclosed by ```) are subject
to certain rules.

Note 2: If subsections code blocks only contain the tag !Initialize!, xix
will initialize the code block with the according current system state. 


## Packages

If this section does not exist or it does not contain any subsections (###), xix
won't manage packages on this system. 

Subsubsections (####) will be ignored. They can be used to temporarily
uninstall certain packages but remember them or to deactivate package
management altogether.

#### Disabled Packages
Example of a disabled package category for illustration purposes. Feel free to
remove this subsubsection.
```
program                 # Arbitrary description
```
You can subcategorize with multiple code blocks.
```
wine                    # Run Microsoft Windows applications
WoeUSB                  # Create a Windows USB stick 
```

### Unsorted Packages
Please sort and create meaningful subsections yourself.
```
!Initialize!
```

## Configurations

This section is mandatory. If it doesn't contain any subsections (###), xix
won't manage configurations on this system. Only the following subsections are
allowed: Shell, Groups, Services, and Global Configs.

Subsubsections (####) will be ignored and can be utilized for temporarily
sidelining certain configurations or for deactivating the configuration
management feature altogether.

### Shell
This setting defines the user's login shell.
```
!Initialize!
```

### Groups
This list defines the user's associated groups exclusively.
```
!Initialize!
```

### Services
This list defines the enabled runit-handled system services exclusively.
```
!Initialize!
```

#### Setup Scripts
Here you can define shell scripts, that run with every xix execution.

Set PAM to handle runtime directory:
```sh
echo "-session optional pam_rundir.so" >> /etc/pam.d/system-login
```
Set the config directory for zsh:
```sh
echo "export ZDOTDIR=/home/$USER_NAME/.config/zsh" >> /etc/zsh/zshenv
```
