#include <asm-generic/errno-base.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>
#include <errno.h>
#include <linux/limits.h>
#include "xix.h"
#include "template.h" // made by cmake from resources/template.md

static void
_make_dir(const char *dir)
{
    char tmp[PATH_MAX];
    char *p = NULL;
    size_t len;

    len = snprintf(tmp, sizeof(tmp),"%s",dir);

    if (tmp[len - 1] == '/') tmp[len - 1] = 0;

    for (p = tmp + 1; *p; p++)
    {
        if(*p == '/')
        {
                *p = 0;
                mkdir(tmp, S_IRWXU);
                *p = '/';
        }
    }
    mkdir(tmp, S_IRWXU);
}

static char*
_get_config_home()
{
    char *config_home = getenv("XDG_CONFIG_HOME");

    if (config_home == NULL)
    {
        // get user home directory
        struct passwd *pw = getpwuid(getuid());
        const char *homedir = pw->pw_dir;
        if (!pw)
        {
            perror("getpwuid failed");
            exit(EXIT_FAILURE);
        }

        // allocate memory
        config_home = malloc(strlen(homedir) + strlen("/.config") + 1);
        if (config_home == NULL)
        {
            perror("Failed to allocate memory for config home");
            exit(EXIT_FAILURE);
        }

        // concatenate strings
        strcpy(config_home, homedir);
        strcat(config_home, "/.config");
    }
    return config_home;
}

static char*
_get_config_dir()
{
    struct stat st = {0};
    const char *config_home = _get_config_home();

    // allocate memory
    char *config_dir = malloc(strlen(config_home) + strlen("/xix/") + 1);
    if (config_dir == NULL)
    {
        perror("Failed to allocate memory for config directory");
    }

    // concatenate strings
    strcpy(config_dir, config_home);
    strcat(config_dir, "/xix/");

    // make directory, if not present
    if (stat(config_dir, &st) == -1)
    {
        printf("Config directory not found. Creating it.\n");
        _make_dir(config_dir);
    }

    return config_dir;
}

static void
_import_template(const char *file_path)
{
    // open file
    FILE *file = fopen(file_path, "wb");
    if (file == NULL)
    {
        perror("Failed to open new config file");
        exit(EXIT_FAILURE);
    }

    // write template
    size_t length = sizeof(config_template) - 1; // exclude null terminator
    size_t written_bytes = fwrite(config_template, 1, length, file);
    if (written_bytes != length)
    {
        perror("Failed to import template");
        exit(EXIT_FAILURE);
    }

    // close file
    fclose(file);
}

static char*
_get_config_file_path()
{
    struct stat st = {0};
    const char *config_dir = _get_config_dir();

    // get hostname
    char hostname[FILENAME_MAX];
    gethostname(hostname, sizeof(hostname));

    // allocate memory
    size_t len = strlen(config_dir) + strlen(hostname) + strlen(".md") + 1;
    char *file_path = malloc(len);
    if (file_path == NULL)
    {
        perror("Failed to allocate memory for config file path");
        exit(EXIT_FAILURE);
    }

    // concatenate strings
    strcpy(file_path, config_dir);
    strcat(file_path, hostname);
    strcat(file_path, ".md");

    // import template, if no config file present
    if (stat(file_path, &st) == -1)
    {
        printf("No config file found, importing template to %s\n", file_path);
        _import_template(file_path);
    }

    return file_path;
}

const char* get_login_shell()
{
    uid_t uid = getuid();
    struct passwd *pw = getpwuid(uid);

    if (pw == NULL) return NULL;

    return pw->pw_shell;
}



char*
read_config_file()
{
    // get config file path
    const char *file_path = _get_config_file_path();

    // open config file
    FILE *file = fopen(file_path, "r");
    if (file == NULL) {
        perror("Could not open config file");
        exit(EXIT_FAILURE);
    }

    fseek(file, 0, SEEK_END);
    long file_size = ftell(file);
    fseek(file, 0, SEEK_SET);

    char *file_data = malloc(file_size + 1);
    if (file_data == NULL)
    {
        perror("Failed to allocate memory for config file data");
        fclose(file);
        exit(EXIT_FAILURE);
    }

    fread(file_data, 1, file_size, file);
    file_data[file_size] = '\0';

    fclose(file);
    return file_data;
}
