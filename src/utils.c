#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "xix.h"

size_t current_line_length(const char *string)
{
    size_t length = 0;
    while (string[length] != '\n' && string[length] != '\0') ++length;
    return length;
}

char* dynamic_strcat(char **dest, const char *src)
{
    size_t dest_len = *dest ? strlen(*dest) : 0;
    size_t src_len = strlen(src);

    // Reallocate memory if needed
    char *new_dest = realloc(*dest, dest_len + src_len + 1); // +1 for the null terminator
    if (!new_dest)
    {
        fprintf(stderr, "Memory allocation failed\n");
        free(*dest);
        *dest = NULL;
        return NULL;
    }

    // Append the new string
    strcpy(new_dest + dest_len, src);
    *dest = new_dest; // Update the original pointer
    return new_dest;
}

char* reallocated(char *string)
{
    char *reallocated_string = realloc(string, strlen(string));
    if (reallocated_string == NULL) return string;
    else return reallocated_string;
}

void advance_to_next_line(const char **string)
{
    while (**string != '\n' && **string != '\0') (*string)++;
    if (**string == '\n') (*string)++;
}

char* allocate_empty_string(size_t size)
{
    char *string = malloc(size + 1);
    if (string == NULL) io_error_exit(MALLOC_ERROR);
    string[0] = '\0';
    return string;
}

size_t get_prefix_length(const char *str)
{
    // size_t line_length = current_line_length(string);
    // const char *prefix_end = strstr(string, " ");
    // size_t word_length = prefix_end - string + 1;
    // if (word_length > line_length) return line_length;
    // return word_length;
    const char *ptr = str;
    while (*ptr != ' ' && *ptr != '\n' && *ptr != '\0')
    {
        ptr++;
    }
    return ptr - str;
}

char* grep_lines(const char *prefix, const char *string)
{
    char *output = allocate_empty_string(strlen(string));
    size_t prefix_lenght = strlen(prefix);

    while (*string != '\0')
    {
        if (strncmp(string, prefix, prefix_lenght) == 0)
        {
            strncat(output, string, current_line_length(string) + 1);
        }
        advance_to_next_line(&string);
    }

    // remove trailing empty line
    const size_t length = strlen(output);
    if (output[length-1] == '\n') output[length-1] = '\0';

    return reallocated(output);
}

int count_lines(const char *string)
{
    int lines = 0;
    for (; *string; string++)
    {
        if (*string == '\n') lines++;
    }
    return lines + 1;
}

void
io_error_exit(const char *error_type)
{
    perror(error_type);
    exit(EXIT_FAILURE);
}
