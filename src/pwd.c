#include <stdbool.h>
#include <stdio.h>
#include <pwd.h>
#include <unistd.h>

struct passwd* get_passwd_db_instance()
{
    static struct passwd data;

    static int is_initialized = false;

    if (is_initialized == false)
    {
        data = getpwuid(getuid());
        if (pw == NULL) {
            fprintf(stderr, "Failed to get passwd entry.\n");
            return NULL;
        }
        userData.login_shell = pw->pw_shell;
        userData.home_directory = pw->pw_dir;
        is_initialized = true;
    }

    return &userData;
}

