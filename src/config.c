#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <xbps.h>
#include "xix.h"

static void initialize_config(struct Config *config)
{
    config->current_pkgs = NULL;
    get_explicitly_installed_pkgs(&config->current_pkgs);
    config->current_shell = NULL;
   // current->groups =
   // current->services = parse_services(config_data);
   // current->scripts = parse_scripts(config_data);
    const char *config_data = read_config_file();
    config->desired_pkgs = parse_packages(config_data);
    config->desired_shell = parse_shell(config_data);
    config->desired_groups = parse_groups(config_data);
    config->desired_services = parse_services(config_data);
    config->desired_scripts = parse_scripts(config_data);
}

struct Config* get_config_instance(int type)
{
    static struct Config config;

    static int config_is_initialized = false;

    if (config_is_initialized == false)
    {
        initialize_config(&config);
        config_is_initialized = true;
    }

    return &config;
}
