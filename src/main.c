#include <stdio.h>
#include <stdlib.h>
#include "xix.h"

int main(int argc, char *argv[])
{
    if(argc > 1)
    {
        printf("Usage: %s [No options currently available!]\n", argv[0]);
        return EXIT_FAILURE;
    }

    struct Config *config = get_config_instance();

    if (handle_packages() == DISABLED)
    {
        printf("Package management disabled.\n");
    }


    if (handle_configurations() == DISABLED)
    {
        printf("configurations management disabled.\n");
    }

    return EXIT_SUCCESS;
}

