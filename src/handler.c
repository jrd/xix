#include <stdlib.h>
#include <stdio.h>
#include "xix.h"




int handle_login_shell()
{
    printf("Handle login shell.\n");
    return 0;
}

int handle_groups()
{
    printf("Handle user groups.\n");
    return 0;
}

int handle_services()
{
    printf("Handle services.\n");
    return 0;
}

int handle_gobal_configs()
{
    printf("Handle glabal configs.\n");
    return 0;
}

int handle_packages()
{
    struct Config *config = get_config_instance();
    printf("acurrent pkgs: %s\n", config->current_pkgs);
    printf("desired pkgs: %s\n", config->desired_pkgs);

    // a) Redundant Packages
    // b) Pending Packages
    // c) Dunious Packages
    return EXIT_SUCCESS;
}

int handle_configurations()
{

    handle_login_shell();

    handle_groups();

    handle_services();

    handle_gobal_configs();

    return EXIT_SUCCESS;
}
