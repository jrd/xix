/*-
 * Copyright (c) 2008-2015 Juan Romero Pardines.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "xix.h"
#include <xbps.h>
#include <string.h>
#include <stdlib.h>

int
list_manual_pkgs(struct xbps_handle *xhp, xbps_object_t obj, const char *key,
                 void *arg, bool *loop_done)
{
    const char *pkgname = NULL;
    bool automatic = false;

    xbps_dictionary_get_bool(obj, "automatic-install", &automatic);

    if (automatic == false)
    {
        xbps_dictionary_get_cstring_nocopy(obj, "pkgname", &pkgname);
        char **pkgsPtr = (char **)arg;
        dynamic_strcat(pkgsPtr, pkgname);
    }

    return 0;
}

int
get_explicitly_installed_pkgs(char** pkgs)
{
    int rv = 0;
    struct xbps_handle xh;
    memset(&xh, 0, sizeof(xh));

    xh.flags = 0;

    if ((rv = xbps_init(&xh)) != 0)
    {
		xbps_error_printf("Failed to initialize libxbps: %s\n", strerror(rv));
		exit(EXIT_FAILURE);
    }

    rv = xbps_pkgdb_foreach_cb(&xh, list_manual_pkgs, pkgs);

    xbps_end(&xh);
	return rv;
}
