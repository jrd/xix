#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include "xix.h"

char*
parse_section(const char *section_name, const char *input)
{
    char *output = allocate_empty_string(strlen(input));
    size_t section_length = strlen(section_name);
    size_t prefix_lenght = get_prefix_length(section_name) +1 ;

    bool in_section = false;

    while (*input != '\0')
    {
        if (strncmp(input, section_name, prefix_lenght) == 0)
        {
            if (strncmp(input, section_name, section_length) == 0)
            {
                in_section = true;
            }
            else
            {
                in_section = false;
            }
        }
        else if (in_section == true)
        {
            strncat(output, input, current_line_length(input) + 1);
        }

        advance_to_next_line(&input);
    }

    return reallocated(output);
}

char*
parse_content_block(const char *block_indicator, const char *input)
{
    char *output = allocate_empty_string(strlen(input));
    size_t block_indicator_lenght = strlen(block_indicator);

    bool in_content_block = false;

    while (*input != '\0')
    {
        if (strncmp(input, block_indicator, block_indicator_lenght) == 0)
        {
            in_content_block = in_content_block ? false : true;
        }
        else if (in_content_block)
        {
            strncat(output, input, current_line_length(input) + 1);
        }

        advance_to_next_line(&input);
    }

    return reallocated(output);
}

char*
parse_first_word_in_line(const char *input)
{
    char *output = allocate_empty_string(strlen(input));
    size_t word_length;
    size_t output_length;

    while (*input != '\0')
    {
        word_length = get_prefix_length(input) + 1;
        strncat(output, input, word_length);
        advance_to_next_line(&input);
    }

    return reallocated(output);
}

char*
parse_packages(const char *config)
{
    char *pkgs_section = parse_section(PKGS_SECTION_NAME, config);
    char *pkgs_subsection = parse_section("### ", pkgs_section);
    char *pkgs_block = parse_content_block(BLOCK_INDICATOR, pkgs_subsection);
    char *pkgs = parse_first_word_in_line(pkgs_block);

    free(pkgs_section);
    free(pkgs_subsection);
    free(pkgs_block);
    return pkgs;
}

char* parse_shell(const char *config)
{
    return "TODO\0";
}

char* parse_groups(const char *config)
{
    return "TODO\0";
}

char* parse_services(const char *config)
{
    return "TODO\0";
}

char* parse_scripts(const char *config)
{
    return "TODO\0";
}
