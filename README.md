**Work in progess!**

# xix

This project is currently under heavy construction. The aim is to develop a
declarative package manager for Void Linux. An initial conceptual script is
available in the 'tests' directory. This script manages packages installed via
XBPS, user-associated groups, runit services, and additional system
configurations through a declarative approach. All specified configurations are
contained within a readable Markdown file.

The name 'xix' has multiple interpretations. One or both of the 'x's likely
stand for 'XBPS'. The 'ix' is intended to echo 'nix,' another declarative
package manager. Meanwhile, the 'i' undoubtedly signifies installation. Feel
free to derive your own meaning from the name.

## Building

```sh
mkdir build
cd build
cmake ..
make
```
